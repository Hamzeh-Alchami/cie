﻿using System;
using System.Net;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.Widget;
using MvvmCross.Binding;
using MvvmCross.Binding.Droid.Target;

namespace MyTemplate.Droid
{
	public class MovieDetailsBackgroundBinding : MvxAndroidTargetBinding
	{
		private readonly LinearLayout _linearLayout;
		public MovieDetailsBackgroundBinding(LinearLayout view) : base(view)
        {
			this._linearLayout = view;
		}
		public override Type TargetType
		{
			get
			{
				return typeof(string);
			}
		}

		protected override void SetValueImpl(object target, object value)
		{

		}

		public override void SetValue(object value)
		{
			var fullPath = "http://image.tmdb.org/t/p/w500" + value;
			var imageBitmap = GetImageBitmapFromUrl(fullPath);
			var s = new BitmapDrawable(imageBitmap);
			s.Alpha = 20;
			_linearLayout.SetBackgroundDrawable(s);

		}

		public override MvxBindingMode DefaultMode
		{
			get { return MvxBindingMode.TwoWay; }
		}
		private Bitmap GetImageBitmapFromUrl(string url)
		{
			Bitmap imageBitmap = null;

			using (var webClient = new WebClient())
			{
				var imageBytes = webClient.DownloadData(url);
				if (imageBytes != null && imageBytes.Length > 0)
				{
					imageBitmap = BitmapFactory.DecodeByteArray(imageBytes, 0, imageBytes.Length);
				}
			}

			return imageBitmap;
		}
	}
}
