using MvvmCross.Platform.Plugins;

namespace MyTemplate.Droid.Bootstrap
{
    public class WebBrowserPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.WebBrowser.PluginLoader>
    {
    }
}