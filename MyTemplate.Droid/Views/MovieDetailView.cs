﻿using Android.App;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Text.Method;
using Android.Widget;
using MvvmCross.Droid.Support.V7.RecyclerView;
using MvvmCross.Droid.Views;
namespace MyTemplate.Droid
{
	[Activity(Label = "MovieDetailView")]
	public class MovieDetailView : MvxActivity<MovieDetailViewModel>
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.MovieDetail);

			this.ActionBar.Hide();

			//make the recyclerview scroll horizentally
			var list = FindViewById<MvxRecyclerView>(Resource.Id.SimilarRecyclerView);
			var manager = new LinearLayoutManager(this, LinearLayoutManager.Horizontal, false);
			list.SetLayoutManager(manager);

			// make movie overview scrollable
			var overview = FindViewById<TextView>(Resource.Id.overviewText);
			overview.MovementMethod=new ScrollingMovementMethod();

		}

		public override void OnBackPressed()
		{
			this.ViewModel.CloseCommand.Execute();
		}


	}
}
