﻿using System;
using Acr.UserDialogs;
using Android.App;
using Android.OS;
using Android.Support.V7.Widget;
using Android.Widget;
using MvvmCross.Droid.Support.V7.RecyclerView;
using MvvmCross.Droid.Views;

namespace MyTemplate.Droid
{
	[Activity(Label = "Cie", Theme = "@style/Theme.AppCompat.NoActionBar")]
	public class HomeView : MvxRefreshActivity
	{
		bool doubleBackToExitPressedOnce;
		protected override void OnCreate(Bundle savedInstanceState)
		{

			UserDialogs.Init(this);
			base.OnCreate(savedInstanceState);
			//this.ActionBar.Hide();

			// Set our view from the "main" layout resource
			SetContentView(Resource.Layout.Home);

			var topRatedList = FindViewById<MvxRecyclerView>(Resource.Id.topRatedList);
			var popularList = FindViewById<MvxRecyclerView>(Resource.Id.popularList);
			var nowplayingList = FindViewById<MvxRecyclerView>(Resource.Id.nowplayingList);

			var topRatedListManager = new LinearLayoutManager(this, LinearLayoutManager.Horizontal, false);
			var popularListManager = new LinearLayoutManager(this, LinearLayoutManager.Horizontal, false);
			var nowplayingListManager = new LinearLayoutManager(this, LinearLayoutManager.Horizontal, false);

			topRatedList.SetLayoutManager(topRatedListManager);
			popularList.SetLayoutManager(popularListManager);
			nowplayingList.SetLayoutManager(nowplayingListManager);

		}

		public override void OnBackPressed()
		{
			if (doubleBackToExitPressedOnce)
			{
				//base.OnBackPressed();
				Java.Lang.JavaSystem.Exit(0);
				return;
			}
			this.doubleBackToExitPressedOnce = true;
			Toast.MakeText(this, "Press again to exit", ToastLength.Short).Show();

			new Handler().PostDelayed(() =>
			{
				doubleBackToExitPressedOnce = false;
			}, 2000);
		}
	}
}
