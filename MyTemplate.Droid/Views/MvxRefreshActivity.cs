﻿using MvvmCross.Droid.Support.V7.AppCompat;

namespace MyTemplate.Droid
{
	public class MvxRefreshActivity : MvxAppCompatActivity
	{
		protected override void OnResume()
		{
			base.OnResume();
			if (ViewModel == null) return;
			((IRefreshData)ViewModel).ReloadCommand.Execute(null);
		}
	}
}
