﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using MvvmCross.Droid.Views;

namespace MyTemplate.Droid
{
	[Activity(Label = "FavoriteView",Theme = "@style/Theme.AppCompat.NoActionBar")]
	public class FavoriteView : MvxRefreshActivity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
			SetContentView(Resource.Layout.Favorite);
		}
	}
}
