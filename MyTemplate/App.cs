﻿using System;
using Acr.UserDialogs;
using MvvmCross.Core.ViewModels;
using MvvmCross.Platform;
using MvvmCross.Platform.IoC;

namespace MyTemplate
{
	public class App : MvxApplication
	{
		public override void Initialize()
		{
			CreatableTypes()
			.EndingWith("Service")
			.AsInterfaces()
			.RegisterAsLazySingleton();
			RegisterAppStart<HomeViewModel>();
			Mvx.RegisterSingleton(() => UserDialogs.Instance);
		}
	}
}
	