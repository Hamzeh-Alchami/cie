﻿using System;
using System.Globalization;
using MvvmCross.Platform.Converters;

namespace MyTemplate
{
	public class ReleaseDateConverter: MvxValueConverter<string, string>
	{
		protected override string Convert(string value, Type targetType, object parameter, CultureInfo culture)
		{
			return StringResources.ReleaseDate + value;
		}
	}
}
