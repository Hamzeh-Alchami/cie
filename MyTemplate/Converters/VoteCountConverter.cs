﻿using System;
using System.Globalization;
using MvvmCross.Platform.Converters;

namespace MyTemplate
{
	public class VoteCountConverter: MvxValueConverter<int, string>
	{
		protected override string Convert(int value, Type targetType, object parameter, CultureInfo culture)
		{
			return  "(From " + value + " votes)";
		}
	}
}
