﻿using System;
using System.Globalization;
using MvvmCross.Platform.Converters;

namespace MyTemplate
{
	public class AppendImageURLConverter: MvxValueConverter<string, string>
	{
		protected override string Convert(string value, Type targetType, object parameter, CultureInfo culture)
		{
			return StringResources.imageUrl + value;
		}
	}
}
