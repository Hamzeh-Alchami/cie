﻿using System;
using System.Globalization;
using MvvmCross.Platform.Converters;

namespace MyTemplate
{
	public class AverageRatingConverter: MvxValueConverter<double, double>
	{
		protected override double Convert(double value, Type targetType, object parameter, CultureInfo culture)
		{
			return value / 2.0;
		}
	}
}
