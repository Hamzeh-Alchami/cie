﻿using System.Collections.Generic;
using System.Linq;
using Acr.UserDialogs;
using MvvmCross.Core.ViewModels;
using MvvmCross.Plugins.WebBrowser;
using Realms;
using TMDbLib.Objects.Movies;
using TMDbLib.Objects.Search;

namespace MyTemplate
{
	public class MovieDetailViewModel:BaseViewModel
	{
		private readonly IMvxWebBrowserTask _webBrowser;
		private int MovieId;
		private Realm _realm;
		private string _movieTitle;
		public string MovieTitle
		{
			get
			{
				return _movieTitle;
			}
			set
			{
				_movieTitle = value;
				RaisePropertyChanged(() => MovieTitle);
			}

		}
		private double _movieRating;
		public double MovieRating
		{
			get
			{
				return _movieRating;
			}
			set
			{
				_movieRating = value;
				RaisePropertyChanged(() => MovieRating);
			}

		}
		private int _voteCount;
		public int VoteCount
		{
			get
			{
				return _voteCount;
			}
			set
			{
				_voteCount = value;
				RaisePropertyChanged(() => VoteCount);
			}

		}
		private string _movieDate;
		public string MovieDate
		{
			get
			{
				return _movieDate;
			}
			set
			{
				_movieDate = value;
				RaisePropertyChanged(() => MovieDate);
			}

		}


		private string _favoriteString;
		public string FavoriteString
		{
			get
			{
				return _favoriteString;
			}
			set
			{
				_favoriteString = value;
				RaisePropertyChanged(() => FavoriteString);
			}

		}

		private string _movieOverview;
		public string MovieOverview
		{
			get
			{
				return _movieOverview;
			}
			set
			{
				_movieOverview = value;
				RaisePropertyChanged(() => MovieOverview);
			}

		}

		private string _movieCover;
		public string MovieCover
		{
			get
			{
				return _movieCover;
			}
			set
			{
				_movieCover = value;
				RaisePropertyChanged(() => MovieCover);
			}

		}

		private string _movieBackgroundURL;
		public string MovieBackgroundURL
		{
			get
			{
				return _movieBackgroundURL;
			}
			set
			{
				_movieBackgroundURL = value;
				RaisePropertyChanged(() => MovieBackgroundURL);
			}

		}
		private List<SearchMovie> _similarList;

		public List<SearchMovie> SimilarList
		{
			get { return _similarList; }
			set
			{
				_similarList = value;
				RaisePropertyChanged(() => SimilarList);
			}
		}
		private readonly IRestService _dataService;
		private readonly IUserDialogs _dialogService;
		private  Movie CurrentMovie;

		public MovieDetailViewModel(IRestService dataService, IUserDialogs dialogs,IMvxWebBrowserTask webBrowser)
		{
			_dataService = dataService;
			_dialogService = dialogs;
			_webBrowser = webBrowser;
			_realm = Realm.GetInstance();
		}

		public async void  Init(int movieId)
		{
			_dialogService.ShowLoading(StringResources.LoadingText, MaskType.Clear);
			CurrentMovie = await _dataService.GetMovieById(movieId);
			SimilarList = await _dataService.GetSimilar(movieId, 1);
			MovieTitle = CurrentMovie.Title;
			MovieDate = CurrentMovie.ReleaseDate.Value.Date.ToString("MM/dd/yyyy");
			MovieOverview = CurrentMovie.Overview;
			MovieCover = CurrentMovie.PosterPath;
			MovieBackgroundURL = CurrentMovie.PosterPath;
			MovieRating = CurrentMovie.VoteAverage;
			VoteCount = CurrentMovie.VoteCount;
			MovieId = movieId;

			//Check if the movie is already favorited
			var isFavorited = _realm.All<Favorites>().Where(d => d.MovieId == CurrentMovie.Id);
			FavoriteString = isFavorited.Count() == 0 ? StringResources.SaveToFavorite : StringResources.Favorited;

			_dialogService.HideLoading();
		}

		public MvxCommand FavoriteCommand
		{
			get
			{
				return new MvxCommand(SaveToRealm);
			}
		}

		void SaveToRealm()
		{
			var isFavorited = _realm.All<Favorites>().Where(d => d.MovieId == CurrentMovie.Id);
			if (isFavorited.Count() == 0)
			{
				_realm.Write(() =>
					{
						var entry = _realm.CreateObject<Favorites>();
						entry.MovieId = CurrentMovie.Id;

					});
				_dialogService.Alert(StringResources.AddedToFavorite, StringResources.Success);
				FavoriteString = StringResources.Favorited;
				Init(MovieId);
			}
			else
			{
				var config = new ConfirmConfig();
				config.Message = StringResources.RemoveMsg;
				config.OkText = StringResources.RemoveString;
				config.CancelText = StringResources.CancelString;
				config.OnAction += OnResult;
				_dialogService.Confirm(config);
			}
		}

		void OnResult(bool obj)
		{
			if (obj)
			{
				var entry = _realm.All<Favorites>().Where(d => d.MovieId == CurrentMovie.Id);
				_realm.Write(() => _realm.Remove(entry.FirstOrDefault()));
				_dialogService.Alert(StringResources.RemoveSuccess);
				FavoriteString = StringResources.SaveToFavorite;
				Init(MovieId);
			}
		}

		public MvxCommand<SearchMovie> SimilarClickedCommand
		{
			get
			{
				return new MvxCommand<SearchMovie>(DoRefresh);
			}
		}
		void DoRefresh(SearchMovie movie)
		{
			Init(movie.Id);
		}

		public MvxCommand PlayTrailerCommand
		{
			get
			{
				return new MvxCommand(PlayVideo);
			}
		}

		async void PlayVideo()
		{
			var TrailerId = await _dataService.GetVideo(MovieId);
			var url = StringResources.YouTubeBaseUrl + TrailerId.Key;
			_webBrowser.ShowWebPage(url);
		}

		public MvxCommand CloseCommand
		{
			get { return new MvxCommand(DoClose); }
		}

		public void DoClose()
		{
			Close(this);
			ShowViewModel<HomeViewModel>();
		}
	}
}