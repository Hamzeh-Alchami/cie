﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Acr.UserDialogs;
using MvvmCross.Binding.ExtensionMethods;
using MvvmCross.Core.ViewModels;
using Realms;
using TMDbLib.Objects.Movies;
using TMDbLib.Objects.Search;

namespace MyTemplate
{
	public class FavoriteViewModel:BaseViewModel,IRefreshData
	{
		private readonly IRestService _dataService;
		private readonly IUserDialogs _dialogService;
		private Realm _realm;
		private ObservableCollection<Movie> _favoriteList;

		public ObservableCollection<Movie> FavoriteList
		{
			get { return _favoriteList; }
			set
			{
				_favoriteList = value;
				RaisePropertyChanged(() => FavoriteList);
			}
		}

		private bool isRefreshing;
		public bool IsRefreshing
		{
			get
			{
				return isRefreshing;
			}
			set
			{
				isRefreshing = value;
				RaisePropertyChanged(() => IsRefreshing);
			}
		}
		private bool isFavoriteEmpty;
		public bool IsFavoriteEmpty
		{
			get
			{
				return isFavoriteEmpty;
			}
			set
			{
				isFavoriteEmpty = value;
				RaisePropertyChanged(() => IsFavoriteEmpty);
			}
		}
		public FavoriteViewModel(IRestService dataService, IUserDialogs dialogs)
		{
			
			_dataService = dataService;
			_dialogService = dialogs;
			_realm = Realm.GetInstance();
		//	GetFavorites();

		}
		async void GetFavorites()
		{
			var Favorites = _realm.All<Favorites>();
			FavoriteList = new ObservableCollection<Movie>();
			IsFavoriteEmpty = Favorites.Count()!=0;
			foreach (var obj in Favorites)
			{
				var movie = await _dataService.GetMovieById(obj.MovieId);
				FavoriteList.Add(movie);

			}
		}

		public MvxCommand<Movie> ShowMovieDetailCommand
		{
			get
			{
				return new MvxCommand<Movie>(ShowMovieDetail);
			}
		}

		void ShowMovieDetail(Movie movie)
		{
			Close(this);
			ShowViewModel<MovieDetailViewModel>(new { movieId = movie.Id });
		}

		public MvxCommand ReloadCommand
		{
			get
			{
				return new MvxCommand(DoReload);
			}
		}

		void DoReload()
		{
			IsRefreshing = true;
			GetFavorites();
			IsRefreshing = false;
		}

}
}
