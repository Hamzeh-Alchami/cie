﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Acr.UserDialogs;
using Cheesebaron.MvxPlugins.Connectivity;
using MvvmCross.Core.ViewModels;
using Realms;
using TMDbLib.Objects.Search;

namespace MyTemplate
{
	public class HomeViewModel: BaseViewModel , IRefreshData
	{
		private Realm _realm;
		private readonly IRestService _dataService;
		private readonly IUserDialogs _dialogService;
		private readonly IConnectivity _connectivity;
		private List<SearchMovie> _topRatedList;
		private List<Movie1> _topRatedTemp;

		private string myTitle;
		public string MyTile
		{
			get { return myTitle; }
			set
			{
				myTitle = value;
				RaisePropertyChanged(() => MyTile);
			}
		}

		public List<SearchMovie> TopRatedList
		{
			get { return _topRatedList;}
			set
			{
				_topRatedList = value;
				RaisePropertyChanged(() => TopRatedList);
			}
		}
		public List<Movie1> TopRatedTemp
		{
			get { return _topRatedTemp; }
			set
			{
				_topRatedTemp = value;
				RaisePropertyChanged(() => TopRatedTemp);
			}
		}

		private List<SearchMovie> _popularList;

		public List<SearchMovie> PopularList
		{
			get { return _popularList; }
			set
			{
				_popularList = value;
				RaisePropertyChanged(() => PopularList);
			}
		}
		private bool isRefreshing;
		public bool IsRefreshing
		{
			get
			{
				return isRefreshing;
			}
			set
			{
				isRefreshing = value;
				RaisePropertyChanged(() => IsRefreshing);
			}
		}
		private List<SearchMovie> _nowplayingList;

		public List<SearchMovie> NowplayingList
		{
			get { return _nowplayingList; }
			set
			{
				_nowplayingList = value;
				RaisePropertyChanged(() => NowplayingList);
			}
		}
		public HomeViewModel(IRestService dataService, IUserDialogs dialogs,IConnectivity connectivity)
		{
			_dataService = dataService;
			_dialogService = dialogs;
			_connectivity = connectivity;
			_realm = Realm.GetInstance();
			MyTile = "dsada";
			LoadMovies();

		}

		private async void LoadMovies()
		{
			//Check internet connection
			if (_connectivity.IsConnected)
			{
				_dialogService.ShowLoading(StringResources.LoadingText, MaskType.Clear);
				TopRatedList = await _dataService.GetTopRated();
				//var templist = new List<Movie1>();
				//foreach (var m in TopRatedList)
				//{
				//	var temp = new Movie1();
				//	temp.PosterPath = m.PosterPath;
				//	templist.Add(temp);
				//}
				//TopRatedTemp = templist;
				myTitle = TopRatedList[0].PosterPath;
				PopularList = await _dataService.GetPopular();
				NowplayingList = await _dataService.GetNowPlaying();
				_dialogService.HideLoading();

				 
			}
			else
				_dialogService.Alert(StringResources.InternetNeeded, "Error");

		}


		public MvxCommand <SearchMovie> ShowMovieDetailCommand
		{
			get
			{
				return new MvxCommand<SearchMovie>(ShowMovieDetail);
			}
		}

		void ShowMovieDetail(SearchMovie movie)
		{
			ShowViewModel<MovieDetailViewModel>(new { movieId = movie.Id});
		}
		public IMvxCommand RefreshCommand
		{
			get
			{
				return new MvxCommand(LoadMovies);
			}
		}

		public MvxCommand ShowFavoriteCommand
		{
			get { return new MvxCommand(DoFavorite); }
		}

		private void DoFavorite()
		{
			ShowViewModel<FavoriteViewModel>();
		}

		public MvxCommand ReloadCommand
		{
			get
			{
				return new MvxCommand(DoReload);
			}
		}

		void DoReload()
		{
			IsRefreshing = true;
			LoadMovies();
			IsRefreshing = false;
		}

	}
}
