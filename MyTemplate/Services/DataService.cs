﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TMDbLib.Client;
using TMDbLib.Objects.General;
using TMDbLib.Objects.Movies;
using TMDbLib.Objects.Reviews;
using TMDbLib.Objects.Search;

namespace MyTemplate
{
	public class DataService : IRestService
	{
		public const string ApiKey = "ab41356b33d100ec61e6c098ecc92140";

		public async Task<List<SearchMovie>> GetNowPlaying()
		{
			try
			{
				var client = new TMDbClient(ApiKey);
				var movies = await client.GetMovieNowPlayingListAsync(null, 1);
				return movies.Results;

			}

			catch (Exception)
			{
				return new List<SearchMovie>();
			}
		}

		public async Task<List<SearchMovie>> GetSimilar(int movieId, int page)
		{
			try
			{
				var client = new TMDbClient(ApiKey);
				var movies = await client.GetMovieSimilarAsync(movieId, page);
				return movies.Results;

			}

			catch (Exception)
			{
				return new List<SearchMovie>();
			}
		}


		public async Task<List<SearchMovie>> GetTopRated()
		{
			try
			{
				var client = new TMDbClient(ApiKey);
				var movies = await client.GetMovieTopRatedListAsync(null, 1);
				return movies.Results;

			}

			catch (Exception)
			{
				return new List<SearchMovie>();
			}
		}

		public async Task<List<SearchMovie>> GetPopular()
		{
			try
			{
				var client = new TMDbClient(ApiKey);
				var movies = await client.GetMoviePopularListAsync(null, 1);
				return movies.Results;

			}

			catch (Exception)
			{
				return new List<SearchMovie>();
			}
		}

		public async Task<Video> GetVideo(int movieId)
		{
			try
			{
				var client = new TMDbClient(ApiKey);
				var movies = await client.GetMovieVideosAsync(movieId);
				return movies.Results.FirstOrDefault();

			}

			catch (Exception)
			{
				return new Video();
			}
		}

		public async Task<Movie> GetMovieById(int movieId)
		{
			try
			{
				var client = new TMDbClient(ApiKey);
				var movie = await client.GetMovieAsync(movieId);
				return movie;

			}

			catch (Exception)
			{
				return new Movie();
			}
		}
	}
}
