﻿using System.Collections.Generic;

namespace MyTemplate
{
	public class Movie1:BindableModel
	{
		public string PosterPath { get; set; }
		public bool IsAdult { get; set; }
		public string Overview { get; set; }
		public string ReleaseDate { get; set; }
		public List<int> GenreIds { get; set; }
		public int Id { get; set; }
		public string OriginalTitle { get; set; }
		public string OriginalLanguage { get; set; }
		public string Title { get; set; }
		public string BackdropPath { get; set; }
		public double Popularity { get; set; }
		public int VoteCount { get; set; }
		public bool Video { get; set; }
		public double VoteAverage { get; set; }
	}

	public class MoviesList
	{
		public int? page { get; set; }
		public List<Movie1> results { get; set; }
		public int? total_results { get; set; }
		public int? total_pages { get; set; }
	}
}
