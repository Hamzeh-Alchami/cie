using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;

namespace MyTemplate
{
    public class BindableModel : INotifyPropertyChanged
    {

		public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public void RaisePropertyChanged<T>(Expression<Func<T>> property)
        {
            var body = property.Body as MemberExpression ?? ((UnaryExpression)property.Body).Operand as MemberExpression;

            RaisePropertyChanged(body.Member.Name);
        }

        public void RaisePropertyChanged(string whichProperty)
        {
            OnPropertyChanged(whichProperty);
        }

    }
}
