﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TMDbLib.Objects.General;
using TMDbLib.Objects.Movies;
using TMDbLib.Objects.Search;

namespace MyTemplate
{
public interface IRestService
	{
		Task<List<SearchMovie>> GetTopRated();

		Task<List<SearchMovie>> GetNowPlaying();

		Task<List<SearchMovie>> GetPopular();

		Task<List<SearchMovie>> GetSimilar(int movieId,int page);

		Task<Video> GetVideo(int movieId);

		Task<Movie> GetMovieById(int movieId);
	}
}
