﻿using MvvmCross.Core.ViewModels;

namespace MyTemplate
{
	public interface IRefreshData
	{
		MvxCommand ReloadCommand { get; }
		bool IsRefreshing { get; set; }

	}
}
