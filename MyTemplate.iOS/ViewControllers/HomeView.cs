﻿using System;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views;
using MvvmCross.iOS.Views;
using TMDbLib.Objects.Lists;
using TMDbLib.Objects.Search;
using UIKit;

namespace MyTemplate.iOS
{
	public partial class HomeView : MvxViewController
	{
		MvxCollectionViewSource src;
		public HomeView() : base("HomeView", null)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			this.NavigationController.NavigationBar.Hidden = true;
			topRatedCollection.RegisterClassForCell(typeof(MoviesCell), MoviesCell.CellId);
			src= new MvxCollectionViewSource(topRatedCollection,MoviesCell.CellId);
			topRatedCollection.Source = src;
			BindUI();

		}

		void BindUI()
		{
			var set = this.CreateBindingSet<HomeView, HomeViewModel>();
			set.Bind(src).For(x=>x.ItemsSource).To(vm => vm.TopRatedList);
			set.Bind(src).For(x => x.SelectionChangedCommand).To(vm => vm.ShowMovieDetailCommand);
			set.Apply();
			topRatedCollection.ReloadData();
		}

		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
			// Release any cached data, images, etc that aren't in use.
		}
	}


	class TopRatedSource : MvxCollectionViewSource
	{
	//	string[] data = { "one", "two", "three", "four","one", "two", "three", "four","one", "two", "three", "four","one", "two", "three", "four" };
		List<Movie1> _itemSource;

		public TopRatedSource(UICollectionView collectionView): base(collectionView)
        {
			collectionView.RegisterClassForCell(typeof(MoviesCell), MoviesCell.CellId);
		}

		public override System.Collections.IEnumerable ItemsSource
		{
			get
			{
				return _itemSource;
			}
			set
			{
				base.ItemsSource = value;
				_itemSource = (List<Movie1>)value;
			}
		}

		public override UICollectionViewCell GetCell(UICollectionView collectionView, NSIndexPath indexPath)
		{
			return (MoviesCell)collectionView.DequeueReusableCell(MoviesCell.CellId, indexPath);
		}

		public override void ItemSelected(UICollectionView collectionView, NSIndexPath indexPath)
		{
			Console.WriteLine("Row {0} selected", _itemSource[indexPath.Row].PosterPath);

		}

		public override bool ShouldSelectItem(UICollectionView collectionView, NSIndexPath indexPath)
		{
			return true;
		}
		public override nint GetItemsCount(UICollectionView collectionView, nint section)
		{
			return base.GetItemsCount(collectionView, section);
		}

		//public override void WillDisplayCell(UICollectionView collectionView, MvxCollectionViewCell cell, NSIndexPath indexPath)
		//{
		//	var cellz = cell as MoviesCell;
		//	cellz.ClearAllBindings();
		//	cellz.BindUi();
		//}
	}

}

