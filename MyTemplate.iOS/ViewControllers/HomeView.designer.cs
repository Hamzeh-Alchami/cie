// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace MyTemplate.iOS
{
    [Register ("HomeView")]
    partial class HomeView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton favoriteButton { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel headerTitle { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView headerview { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView moviesView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UICollectionView topRatedCollection { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (favoriteButton != null) {
                favoriteButton.Dispose ();
                favoriteButton = null;
            }

            if (headerTitle != null) {
                headerTitle.Dispose ();
                headerTitle = null;
            }

            if (headerview != null) {
                headerview.Dispose ();
                headerview = null;
            }

            if (moviesView != null) {
                moviesView.Dispose ();
                moviesView = null;
            }

            if (topRatedCollection != null) {
                topRatedCollection.Dispose ();
                topRatedCollection = null;
            }
        }
    }
}