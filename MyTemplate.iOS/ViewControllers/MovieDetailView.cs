﻿using System;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Binding.iOS.Views;
using MvvmCross.iOS.Views;
using UIKit;

namespace MyTemplate.iOS
{
	public partial class MovieDetailView : MvxViewController
	{
		private  MvxImageViewLoader _imageLoader;
		public MovieDetailView() : base("MovieDetailView", null)
		{
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();
			NavigationController.NavigationBar.Hidden = true;
			_imageLoader = new MvxImageViewLoader(() => DetailCover);
			BindUI();
		}

		void BindUI()
		{
			var set = this.CreateBindingSet<MovieDetailView, MovieDetailViewModel>();
			set.Bind(_imageLoader).For(x => x.ImageUrl).To(item => item.MovieCover).WithConversion(ConverterNames.AppendImageURL);
			set.Bind(movieSummary).For(x => x.Text).To(item => item.MovieOverview);
			set.Apply();
		}
		public override void DidReceiveMemoryWarning()
		{
			base.DidReceiveMemoryWarning();
		}
	}
}

