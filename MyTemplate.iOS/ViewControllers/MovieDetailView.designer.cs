// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace MyTemplate.iOS
{
    [Register ("MovieDetailView")]
    partial class MovieDetailView
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIImageView DetailCover { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView middleView { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UITextView movieSummary { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIView topView { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (DetailCover != null) {
                DetailCover.Dispose ();
                DetailCover = null;
            }

            if (middleView != null) {
                middleView.Dispose ();
                middleView = null;
            }

            if (movieSummary != null) {
                movieSummary.Dispose ();
                movieSummary = null;
            }

            if (topView != null) {
                topView.Dispose ();
                topView = null;
            }
        }
    }
}