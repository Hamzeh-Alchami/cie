using MvvmCross.Platform.Plugins;

namespace MyTemplate.iOS.Bootstrap
{
    public class WebBrowserPluginBootstrap
        : MvxLoaderPluginBootstrapAction<MvvmCross.Plugins.WebBrowser.PluginLoader, MvvmCross.Plugins.WebBrowser.iOS.Plugin>
    {
    }
}