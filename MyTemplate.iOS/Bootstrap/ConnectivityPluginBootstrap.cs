using MvvmCross.Platform.Plugins;

namespace MyTemplate.iOS.Bootstrap
{
    public class ConnectivityPluginBootstrap
        : MvxLoaderPluginBootstrapAction<Cheesebaron.MvxPlugins.Connectivity.PluginLoader, 
			Cheesebaron.MvxPlugins.Connectivity.Touch.Plugin> { }
}