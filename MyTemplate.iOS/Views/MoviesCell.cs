﻿using Foundation;
using System;
using UIKit;
using System.Drawing;
using CoreGraphics;
using MvvmCross.Binding.iOS.Views;
using MvvmCross.Binding.BindingContext;
using TMDbLib.Objects.Search;

namespace MyTemplate.iOS
{
	public class MoviesCell : MvxCollectionViewCell
	{
		public UIImageView movieCover;
		public UILabel movieCover2;
		private readonly MvxImageViewLoader _imageLoader;
		public static readonly NSString CellId = new NSString("topRatedCell");
		public MoviesCell(IntPtr handle)
			: base(string.Empty,handle)
		{
			movieCover = new UIImageView(new CGRect(0, 0, 120, 120));
			movieCover2 = new UILabel(new CGRect(0, 0, 120, 120));
			movieCover2.TextColor = UIColor.Red;
			movieCover2.BackgroundColor = UIColor.Blue;
			//	movieCover2.Text = "dasdasdas";
			Add(movieCover);
		//	Add(movieCover2);
			//ContentView.AddSubview(movieCover2)
			_imageLoader = new MvxImageViewLoader(() => this.movieCover);
			BindUi();

		}
		public void BindUi()
		{
			this.DelayBind(() =>
			{
				var set = this.CreateBindingSet<MoviesCell, SearchMovie>();
				set.Bind(_imageLoader).For(x => x.ImageUrl).To(item => item.PosterPath).WithConversion(ConverterNames.AppendImageURL);
				//set.Bind(movieCover2).For(p => p.Text).To(vm => vm.PosterPath);
				set.Apply();
			});
		}

	}
}